import javax.imageio.*;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class IIOImageExample {
    public static void main(String[] args) {
        try {
            // Read an image file
            BufferedImage image = ImageIO.read(new File("input.jpg"));

            // Create an IIOImage object with the buffered image
            IIOImage iioImage = new IIOImage(image, null, null);

            // Write the IIOImage object to an output file
            File outputFile = new File("output.jpg");
            ImageWriter writer = ImageIO.getImageWritersByFormatName("JPEG").next();
            ImageOutputStream outputStream = ImageIO.createImageOutputStream(outputFile);
            writer.setOutput(outputStream);

            // Set compression quality
            JPEGImageWriteParam writeParam = (JPEGImageWriteParam) writer.getDefaultWriteParam();
            writeParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            writeParam.setCompressionQuality(0.8f);

            // Write the image with metadata
            writer.write(null, iioImage, writeParam);

            // Close the writer and output stream
            writer.dispose();
            outputStream.close();

            System.out.println("Image saved successfully.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
