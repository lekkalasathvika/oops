// Parent class
class Animal {
    private String name = "Animal";

    public void printName() {
        System.out.println(name);
    }
}

// Private inheritance
class Cat extends Animal {
}

// Protected inheritance
class Dog extends Animal {
    protected String name = "Dog";
}

// Public inheritance
class Horse extends Animal {
    public String name = "Horse";
}

public class pppinheri {
    public static void main(String[] args) {
        // Private inheritance
        Cat cat = new Cat();
        cat.printName(); // Prints "Animal"
        //System.out.println(cat.name); // Compilation error

        // Protected inheritance
        Dog dog = new Dog();
        dog.printName(); // Prints "Animal"
        System.out.println(dog.name); // Accessible within the same package or through inheritance

        // Public inheritance
        Horse horse = new Horse();
        horse.printName(); // Prints "Animal"
        System.out.println(horse.name); // Accessible everywhere
    }
}

