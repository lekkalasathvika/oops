import java.util.*;
public class ConstDest1{
    String fullName;
    int rollNum;
    double semPercentage;
    String collegeName;
    int collegeCode;
    ConstDest1(String name,String cName,int idNum,int cCode,double spercentage){
        fullName=name;
        rollNum=idNum;
        semPercentage=spercentage;
        collegeName=cName;
        collegeCode=cCode;
    }
    void display()
    {
        System.out.println("Full Name:"+fullName);
        System.out.println("Roll Number:"+rollNum);
        System.out.println("Sem Percentage:"+semPercentage);
        System.out.println("college name:"+collegeName);
        System.out.println("College Code:"+collegeCode);
    }
    protected void finalize() throws Throwable
    {
       System.out.println("Object destroyed");
    } 
    
public static void main(String[]args)
{
    System.out.println("Student Details:");
    Scanner input = new Scanner(System.in);
    System.out.println("Enter your college Name: ");
    String cName = input.nextLine();
    System.out.println("Enter your name: ");
    String name= input.nextLine();
    System.out.println("Enter your roll number: ");
    int idNum = input.nextInt();
    System.out.println("Enter your sem percentage: ");
    double spercentage = input.nextDouble();
    System.out.println("Enter your college code: ");
    int cCode = input.nextInt();
    ConstDest1 obj = new ConstDest1(name,cName,idNum,cCode,spercentage);
    obj.display();
}
}
